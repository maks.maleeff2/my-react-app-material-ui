import script from './index';

// @ts-ignore
if (module.hot) {
  // @ts-ignore
  module.hot.accept('./index', script);
}

script();
