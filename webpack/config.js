const path = require('path');
const resolve = require('resolve');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const typescriptFormatter = require('react-dev-utils/typescriptFormatter');
const ForkTsCheckerWebpackPlugin = require('react-dev-utils/ForkTsCheckerWebpackPlugin');

const paths = {
  build: path.join(__dirname, '../build'),
  source: path.join(__dirname, '../source'),
  entry: path.join(__dirname, '../source/runner'),
  html: path.join(__dirname, '../source/index.html')
};

const output = {
  publicPath: '/',
  path: paths.build,
  filename: 'index.js'
};

const scriptLoader = {
  test: /\.(js)$/,
  loader: require.resolve('babel-loader')
};

const fileLoader = {
  test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.svg$/, /\.png$/],
  loader: 'url-loader',
  options: {
    limit: 10000,
    name: 'media/[name].[hash:8].[ext]'
  }
};

const styleLoader = {
  test: /\.scss$/i,
  use: [
    false ? MiniCssExtractPlugin.loader : 'style-loader',
    'css-loader',
    {
      loader: 'postcss-loader',
      options: {
        ident: 'postcss',
        plugins: () => [
          require('postcss-flexbugs-fixes'),
          require('postcss-preset-env')({
            autoprefixer: {
              flexbox: 'no-2009'
            },
            stage: 3
          }),
          require('postcss-normalize')()
        ]
      }
    },
    'sass-loader'
  ]
};

module.exports = mode => ({
  mode,
  output,
  entry: paths.entry,
  module: {
    rules: [scriptLoader, fileLoader, styleLoader]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: paths.html
    })
  ]
});
