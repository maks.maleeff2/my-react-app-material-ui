const extensions = ['.js', '.jsx', '.ts', '.tsx'];
require('@babel/register')({ extensions });

const ip = require('ip');
const chalk = require('chalk');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const config = require('./config');
const isInteractive = process.stdout.isTTY;
const openBrowser = require('react-dev-utils/openBrowser');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');
const evalSourceMapMiddleware = require('react-dev-utils/evalSourceMapMiddleware');
const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware');

const HOST = process.env.HOST || '0.0.0.0';
const PORT = parseInt(process.env.PORT, 10) || 3231;
const PROTOCOL = process.env.HTTPS === 'true' ? 'https' : 'http';

const browserIpUrl = `${PROTOCOL}://${HOST}:${PORT}`;
const browserLocalHostUrl = `${PROTOCOL}://${'localhost'}:${PORT}`;

const LOG = (message, color = 'green') => console.log(chalk[color](message));

const MODE = 'development';

const serverConfig = {
  // host: '0.0.0.0',
  // proxy: {},
  // quiet: true,
  // compress: true,
  // overlay: false,
  // watchContentBase: true,
  // historyApiFallback: { disableDotRule: true },

  // contentBase: path.join(__dirname, ''),
  // port: 9000
  // watchOptions: {
  //   ignored: /^(?!\/Applications\/MAMP\/htdocs\/projects\/multiplayer\.web\/source\/).+\/node_modules\//g
  // },
  // publicPath: '/',
  // public: '192.168.88.65',
  // clientLogLevel: 'none'
  // contentBase:
  //  '/Applications/MAMP/htdocs/projects/europaplus.web/source/app/client/assets',
  // https: false,
  // disableHostCheck: false,
  //   before(server, app) {
  //   console.log(server, app);

  //     // app.use(evalSourceMapMiddleware(server));
  //     // app.use(errorOverlayMiddleware());
  //     // app.use(noopServiceWorkerMiddleware());
  //   }

  hot: true,
  compress: true,
  clientLogLevel: "silent",
  stats: {
    colors: true,
    hash: false,
    version: false,
    timings: false,
    assets: false,
    chunks: false,
    modules: false,
    reasons: false,
    children: false,
    source: false,
    errors: false,
    errorDetails: false,
    warnings: false,
    publicPath: false
  }
};

function build(previousFileSizes) {
  // if (process.env.NODE_PATH) {
  //   console.log(chalk.yellow('Setting NODE_PATH to resolve modules absolutely has been deprecated in favor of setting baseUrl in jsconfig.json (or tsconfig.json if you are using TypeScript) and will be removed in a future major release of create-react-app.'));
  //   console.log();
  // }

  LOG(`\n\nCreating an optimized production build...`, 'cyan');
  console.log();

  const compiler = webpack(config(MODE));
  return new Promise((resolve, reject) => {
    compiler.run((err, stats) => {
      let messages;
      if (err) {
        if (!err.message) {
          return reject(err);
        }
        messages = formatWebpackMessages({
          errors: [err.message],
          warnings: []
        });
      } else {
        messages = formatWebpackMessages(
          stats.toJson({ all: false, warnings: true, errors: true })
        );
      }
      if (messages.errors.length) {
        // Only keep the first error. Others are often indicative
        // of the same problem, but confuse the reader with noise.
        if (messages.errors.length > 1) {
          messages.errors.length = 1;
        }
        return reject(new Error(messages.errors.join('\n\n')));
      }
      if (
        process.env.CI &&
        (typeof process.env.CI !== 'string' ||
          process.env.CI.toLowerCase() !== 'false') &&
        messages.warnings.length
      ) {
        console.log(
          chalk.yellow(
            '\nTreating warnings as errors because process.env.CI = true.\n' +
              'Most CI servers set it automatically.\n'
          )
        );
        return reject(new Error(messages.warnings.join('\n\n')));
      }

      return resolve({
        stats,
        previousFileSizes,
        warnings: messages.warnings
      });
    });
  });
}

;

WebpackDevServer.addDevServerEntrypoints(config, serverConfig);
const devServer = new WebpackDevServer(webpack(config(MODE)), serverConfig);

devServer.listen(PORT, HOST, err => {
  if (err) return console.log(err);
  if (isInteractive) console.clear();
  LOG('Starting the development server...\n', 'cyan');

  openBrowser(browserLocalHostUrl);
});
// build(0)
['SIGINT', 'SIGTERM'].forEach(sig => {
  process.on(sig, () => {
    devServer.close();
    process.exit();
  });
});
